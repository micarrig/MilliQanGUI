from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *
#from MilliDAQ.python.Demonstrator import *
#from Demonstrator import *
import sys
import logging
import subprocess
import os
import signal
import time
import glob
import select
import os.path



def btn(self, name, x, y, connect, colors=[]):
	btnnew = QPushButton(name,self)
	btnnew.move(x,y)
	btnnew.clicked.connect(connect)
	if len(colors)>1:
		btnnew.setStyleSheet("QPushButton"
				"{"
				"background-color : " + colors[0] + ";"
				"}"
				"QPushButton::pressed"
				"{"
				"background-color : " + colors[1] + ";"
				"}"
		)
	return btnnew
	
	
def btnlongrun(self, name, x, y, connect, colors=[]):
	btnnew = QPushButton(name,self)
	btnnew.move(x,y)
	btnnew.clicked.connect(connect)
	if len(colors)>1:
		btnnew.setStyleSheet("QPushButton"
			     "{"
			     "background-color : " + colors[0] + ";"
			     "}"
			     "QPushButton::pressed"
			     "{"
			     "background-color : " + colors[1] + ";"
			     "}"
		)
	return btnnew
	
def btn_size(self,name,x,y,connect,x1,y1):
	newbtn = QPushButton(name,self)
	newbtn.resize(x1,y1)
	newbtn.move(x,y)
	newbtn.clicked.connect(connect)
	
def setlabel(self, text,x,y):
	label = QLabel(self)
	label.setText(text)
	label.move(x,y)
	label.adjustSize()
	
